package week1.day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCard {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		String next = sc.next();
//		String data = "1234A5678B9012C0987";
		String template = "[0-9]{4}[A-Z]{1}[0-9]{4}\\w[0-9]{4}.\\d{4}";
		
		Pattern pat = Pattern.compile(template);
		Matcher match = pat.matcher(next);
		System.out.println("The given data is matched with the pattern: " +match.matches());
		
		System.out.println((next.replaceAll("\\D", " ")));
		sc.close();
	}

}
