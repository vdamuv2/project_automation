package week1.day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateEmail {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String template = "[A-Za-z]{7,10}.[A-Z]{1}\\W[A-Za-z]{10}[^\\w][a-z]{3}";
		
//		String email = "Vinitha.V@lionbridge.com";
//		String template = "[A-Za-z]{7,10}.[A-Z]{1}\\W[A-Za-z]{10}[^\\w][a-z]{3}";
		
		Pattern pat = Pattern.compile(template);
		Matcher match = pat.matcher(sc.nextLine());
		System.out.println(match.matches());
		
		sc.close();
	}

}
