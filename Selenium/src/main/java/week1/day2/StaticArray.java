package week1.day2;

public class StaticArray {

	public static void main(String[] args) {
	int[] staticArray1 = {43,55,66,87,66,44,33,22};
	System.out.println("Display the 3 index of the array:" +staticArray1[3]);

	String staticArray2[] = {"Rani", "Raja", "King", "Queen"};
	System.out.println("Display the 2nd index of the array:" +staticArray2[2]);
	System.out.println("expection thrown if array size is out of index" +staticArray2[6]);
}

}