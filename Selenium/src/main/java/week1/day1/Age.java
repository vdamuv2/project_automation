package week1.day1;

import java.util.Scanner;

public class Age {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int nextInt = sc.nextInt();
		getAge(nextInt);
		sc.close();
	}

	public static void getAge(int nextInt)
	{
		if(nextInt<=20)
		{
			System.out.println("You are not eligible for voting!");
		}else
		{
		System.out.println("You are eligible for voting!");
		}	
	}
}
