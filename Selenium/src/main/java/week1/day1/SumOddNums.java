package week1.day1;

import java.util.Scanner;

public class SumOddNums {
      public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
          
            System.out.print("Enter a your number: ");
            
          int n = in.nextInt();   
      	  int reminder = 0;
                  while (n != 0) 
                  {
                	  reminder = n % 10;
                	  if(reminder!=reminder%2)
                	  {
                		  reminder += n % 10;
                	  }
                        n /= 10;
                  }
                  System.out.println("Sum: " + reminder);
                  in.close();
        }
}
