package week1.day1;

import java.util.Scanner;

public class FindDays {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int nextInt = sc.nextInt();
		getDays(nextInt);
		sc.close();
	}
	
	public static int getDays(int nextInt)
	{
		switch(nextInt)
		{
		case 0: 
		{
			System.out.println("Sunday");
			break;
		}
		case 1: 
		{
			System.out.println("Monday");
			break;
		}
		case 2: 
		{
			System.out.println("Tuesday");
			break;
		}
		case 3: 
		{
			System.out.println("Wednesday");
			break;
		}
		case 4: 
		{
			System.out.println("Thursday");
			break;
		}
		case 5: 
		{
			System.out.println("Friday");
			break;
		}
		case 6: 
		{
			System.out.println("Saturday");
			break;
		}
		default: 
		{
			System.out.println("Invalid");
			break;
		}
		}
		return 0;
	}

}
