package week1.day1;

public class MyPhone {

	public static void main(String[] args) {
		MobilePhone mobile = new MobilePhone();
		boolean callStatusRinging = mobile.callStatusRinging(true);
		System.out.println("The call status is:" +callStatusRinging);
		
		boolean callStatusRinging1 = mobile.callStatusRinging(false);
		System.out.println("The call status again is:" +callStatusRinging1);
		
		String message = mobile.getMessage("Hello All", 1225655);
		System.out.println("The message is:" +message);
		
		System.out.println(mobile.mobileColor);
		System.out.println(mobile.mobileSeries);
		
		
	}

}
