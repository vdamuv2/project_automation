package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().frame("iframeResult"); //id of the frame is used
		driver.findElementByXPath("//button[text()='Try it']").click();
		String text = driver.switchTo().alert().getText();
		WebDriverWait wait = new WebDriverWait(driver,10);
		Alert presentAlert = wait.until(ExpectedConditions.alertIsPresent());
//		System.out.println(presentAlert);
		System.out.println("The text found in the alert popup is: " +text);
		driver.switchTo().alert().sendKeys("Vinitha");
		driver.switchTo().alert().accept();
		WebElement sentences = driver.findElementByXPath("//p[@id='demo']");
		String myName = sentences.getText();
		System.out.println("Sentences found inside the frame is: " +myName);
		if(myName.contains("Vinitha"))
		{
			System.out.println("'Vinitha' is found in the sentences");
		}
		else System.out.println("Invalid name entered");
		}
}
