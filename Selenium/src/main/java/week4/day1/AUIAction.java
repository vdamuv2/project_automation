package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class AUIAction {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
//		driver.switchTo().alert().dismiss();
		
		Actions keyboardClick = new Actions(driver);
		keyboardClick.keyDown(Keys.ESCAPE).perform();
		WebElement electronics = driver.findElementByXPath("//span[contains(text(),'Electronics')]");
		WebElement apple = driver.findElementByXPath("//a[text()='Apple']");
		keyboardClick.moveToElement(electronics).pause(20).click(apple).perform();
		System.out.println("done");

	}

}
