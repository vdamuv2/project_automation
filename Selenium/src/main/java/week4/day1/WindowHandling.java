package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/pageUnderConstruction.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windows = driver.getWindowHandles(); //getting total no of windows
		List<String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(windows); // adding all windows in the list
		driver.switchTo().window(listOfWindows.get(1)); //switching to the particular window
 		System.out.println(driver.getTitle()); // working inside the 2nd window OR accessing the elements in 2nd window
 		driver.quit();
	}

}
