package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeHomeWork {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesMAnager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[contains(text(),'Merge Leads')]").click();
		driver.findElement(By.xpath("//input[@name='partyIdFrom']/following::a[1]")).click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listAllWindows = new ArrayList<>();
		listAllWindows.addAll(windowHandles);
		driver.switchTo().window(listAllWindows.get(1));
		driver.findElementByXPath("//div[@class='x-form-element']/input[@name='id']").sendKeys("10083",Keys.ENTER);
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext' and text()='10083']").click();
		driver.switchTo().window(listAllWindows.get(0)); //switching back to main/1st window
		
		driver.findElement(By.xpath("//a[text()='Merge']/parent::td/preceding-sibling::td/parent::tr/preceding-sibling::tr//input[@id='partyIdTo']/following::a[1]")).click();
		Set<String> windowHandles1 = driver.getWindowHandles();
		List<String> listAllWindows1 = new ArrayList<>();
		listAllWindows1.addAll(windowHandles1);
		driver.switchTo().window(listAllWindows1.get(1));
		driver.findElementByXPath("//div[@class='x-form-element']/input[@name='id']").sendKeys("10086",Keys.ENTER);
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext' and text()='10086']").click();
		driver.switchTo().window(listAllWindows1.get(0)); //switching back to main/1st window
		driver.findElementByXPath("//a[text()='Merge']").click();
		
		Alert simpleAlert = driver.switchTo().alert();
		System.out.println(simpleAlert.getText());
		simpleAlert.accept();
		
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys("10083",Keys.ENTER);
		
		String  expectedVerification =driver.findElementByXPath("//div[text()='No records to display']").getText();
		System.out.println(expectedVerification);
		String actualVerificationTxt = "no recORds To disPlaY";
		if(actualVerificationTxt.equalsIgnoreCase(expectedVerification))
		{
			System.out.println("Verified the expected 'No records to display' text message is found");
		}
		driver.close();
	}


}
