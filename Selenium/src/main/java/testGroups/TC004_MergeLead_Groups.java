package testGroups;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import wdmethods.ProjectMethods;

public class TC004_MergeLead_Groups extends ProjectMethods {
	
	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}
	
//	@Test(priority = 2)
//	@Test(dataProvider = "fetch")
	@Test(groups="Smoke")
	public void mergeLead(String num1, String num2 )
	{
		WebElement eleLead = locateElement("linkText", "Leads");
		click(eleLead);
		
		WebElement eleMerge = locateElement("xpath", "//a[contains(text(),'Merge Leads')]");
		click(eleMerge);
		
		WebElement eleFromIcon = locateElement("xpath", "//input[@name='partyIdFrom']/following::a[1]");
		click(eleFromIcon);
		
		handleMultipleWindows(1);
		WebElement eleFromId = locateElement("xpath", "//div[@class='x-form-element']/input[@name='id']");
		type(eleFromId, num1);
		WebElement eleButton = locateElement("xpath","//button[text()='Find Leads']");
		click(eleButton);
		WebElement eleClick = locateElement("xpath","//a[@class='linktext' and text()='10083']");
		waitTime();
		click(eleClick);
		
		waitTime();
		handleMultipleWindows(1);
		WebElement eleToId = locateElement("xpath", "//a[text()='Merge']/parent::td/preceding-sibling::td/parent::tr/preceding-sibling::tr//input[@id='partyIdTo']/following::a[1]\"");
		type(eleToId, num2);
		WebElement eleButtons = locateElement("xpath","//button[text()='Find Leads']");
		click(eleButtons);
		WebElement eleClickTo = locateElement("xpath","//a[@class='linktext' and text()='10071']");
		waitTime();
		click(eleClickTo);
	
	}
	
	@DataProvider(name = "fetch")
	public String[][] getData()
	{
		String[][] data = new String[2][2];
		data[0][0] = "10083";
		data[0][1] = "10088";
		
		data[1][0] = "10071";
		data[1][1] = "10077";
		
		
		return data;
		
	}
	
	@AfterMethod
	public void CloseApp()
	{
		closeBrowser();
	}
}
