package testGroups;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import wdmethods.ProjectMethods;

public class TC001_CreateLead_Groups extends ProjectMethods {

	@BeforeTest
	public void setData(String testName, String tesDesc,  String tAuthor, String tcategory) {
		testcaseName = "test";
		testDesc = "te";
		author = "vini";
		category = "smoke";
	}

	@Parameters({ "browser", "url" })
	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}

//	@Test(invocationCount = 2)
//	@Test(invocationCount = 2, invocationTimeOut = 8000)
//	@Test(priority = 5)
//	@Test(dataProvider = "fetchData")
	@Test(groups="Smoke")
	public void createL(String comName, String fName, String lName) {
		WebElement eleLink = locateElement("linkText", "Create Lead");
		click(eleLink);

		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, comName);

		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, fName);

		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, lName);

		WebElement dropdown = locateElement("name", "marketingCampaignId");
		selectDropDownUsingText(dropdown, "Automobile");

		WebElement dropdown2 = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(dropdown2, 3);

		WebElement dropdown3 = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingValue(dropdown3, "OWN_PROPRIETOR");

		WebElement importantNotes = locateElement("name", "importantNote");
		type(importantNotes, "Hello Everyone!!! Good Morning!!!");

		WebElement eleButton = locateElement("name", "submitButton");
		click(eleButton);

	}

	@DataProvider(name = "fetchData")
	public String[][] getData()
	{
		String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "Vinitha";
		data[0][2] = "Vinayan";
		
		data[1][0] = "Wipro";
		data[1][1] = "Vinitha";
		data[1][2] = "Damodharan";
		return data;
		
	}
	@AfterMethod
	public void CloseApp() {
		closeBrowser();
	}

}
