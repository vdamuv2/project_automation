package ReportFiles;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporter {
	public static ExtentReports report;
	public static ExtentHtmlReporter html;
	public static ExtentTest testcaseInfo;
	@BeforeTest
	public void createReports()
	{
	html = new ExtentHtmlReporter("./Reports/result.html"); // a blank HTML templace is created
	html.setAppendExisting(true); // it helps to maintain the number of execution reports, it appends all the reports
	report = new ExtentReports(); //a blank report is generated
	report.attachReporter(html); //it attach the report to the html
	}
	
	public void reporter(String testcaseName, String desc)
	{
		testcaseInfo = report.createTest(testcaseName, desc);
		testcaseInfo.assignAuthor("Vinitha");
		testcaseInfo.assignCategory("Smoke");
	}
	@Test
	public void reports() 
	{
		
		try {
			testcaseInfo.pass("Step No: 1 PASSED", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
//MediaEntityBuilder.createScreenCaptureFromPath will throw IOException since img.png may or may not present in the folder so we use IOException to compromise the complier that the img may or maynot present dont throw an exception.
//error thrown for MediaEntityBuilder.createScreenCaptureFromPath is complie time error.
		} catch (IOException e) {
			System.out.println("Exception");
		}
	}
	
	@AfterTest
	public void stopReports()
	{
		report.flush();
	}
}
