package project1;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.NoInjection;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class ZoomCar {

//	@BeforeMethod
//	public void loginApp(String bro, String zoomUrl) {
//		login(bro, zoomUrl);
//	}

	@Test
	public void zoomPrice() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai/search");
		driver.findElementByXPath("//div[@class='component-popular-locations']/div[2]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//div[@class='days']//following::div[contains(text(),'show more')]").click();
		Thread.sleep(3000);
//	List<WebElement> dates = driver.findElementsByXPath("//div[@class='calendar']");
//	WebElement findTomorrow = driver.findElementByXPath("//ul[@class='days']/li[@class='picked full']/following::li[1]");
		WebElement findTomorrow = driver.findElementByXPath("//ul[2]/li[21]");
		String selectTomo = findTomorrow.getText();
		findTomorrow.click();
//	LocalDateTime findToday = LocalDateTime.now();
//	String tomoDay = findTomorrow.getText();
//	System.out.println("Today date is: " +findToday);
//	System.out.println("Tomorrow selected date is: " +tomoDay);
		Thread.sleep(3000);
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(3000);
		WebElement verifyTomorrow = driver.findElementByXPath("//div[@class='days']/div[1]");
		String textTmrw = verifyTomorrow.getText();
		if (selectTomo.equals(textTmrw)) {
			System.out.println("The selected date is verified");
		}

		driver.findElementByXPath("//button[text()='Done']").click();
		Thread.sleep(3000);
//		WebDriverWait waitSpecific = new WebDriverWait(driver, 50);
//		List<WebElement> findElementsByXPath = driver.findElementsByXPath("//div[@class='car-list-layout']");
//		waitSpecific.until(ExpectedConditions.visibilityOf(findElementsByXPath));

//		List<WebElement> listOfCars = driver.findElementsByXPath("//div[@class='car-list-layout']/div");

		List<WebElement> list = new ArrayList<WebElement>();
		List<WebElement> listOfCarPrice = driver.findElementsByXPath("//div[@class='price']");
		list.addAll(listOfCarPrice);

		for (WebElement highPrice : listOfCarPrice) {
			String firstValue = highPrice.getText();
			String replaceRegex = firstValue.replaceAll("₹", ""); // removing the regex symbol from the string
			int firstValueNum = Integer.parseInt(replaceRegex.trim()); // converting string into int
			String secondValue = highPrice.getText();
			String replaceRegexs = secondValue.replaceAll("₹", "");
			int secondValueNum = Integer.parseInt(replaceRegexs.trim()); // converting string into int
			if (!firstValue.isEmpty()) {
				if (firstValueNum > secondValueNum) {
					int temp = firstValueNum;
					secondValueNum = firstValueNum;
//					if (secondValueNum > firstValueNum) {
						int temp2 = secondValueNum;
						if (temp > temp2) {
							System.out.println(temp);
							temp = secondValueNum;
						} else
							System.out.println(temp2);
						secondValueNum = temp2;
					}
					secondValueNum++;
//				}
			}
		}

		driver.close();
	}
}
