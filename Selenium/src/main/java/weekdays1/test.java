package weekdays1;

import java.util.Scanner;

public class test {

	public static void main(String[] args) {
				Scanner sc = new Scanner(System.in);
			//	ArraySorting obj = new ArraySorting();
				System.out.println("Enter the size of array(Min Three Values :)");
				int size = sc.nextInt();
				int inpArr[] = new int[size];
				for (int i = 0; i < size; i++) {
					System.out.println("Enter the values for array");
					inpArr[i] = sc.nextInt();
				}
				System.out.println("Sorted Array is (Ascending order) :");
//				obj.sortedArrayAsc(inpArr);
				for (int eachValue : inpArr) {
					System.out.print(eachValue+",");
				}
				System.out.println();
				System.out.println("Sorted Array is (Descending order) :");
//				obj.sortedArrayDesc(inpArr);
				for (int eachValue : inpArr) {
					System.out.print(eachValue+",");
				}
				System.out.println();
				System.out.println("First and Third Largest number in given array is :"+inpArr[0]+" and "+inpArr[2]);

			}

			public int[] sortedArrayAsc(int inpArr[]) {
				int temp = 0;
				for (int i = 0; i < inpArr.length; i++) {
					for (int j = i + 1; j < inpArr.length; j++) {
						if (inpArr[i] > inpArr[j]) {
							temp = inpArr[i];
							inpArr[i] = inpArr[j];
							inpArr[j] = temp;
						}
					}
				}
				return inpArr;
			}

			public int[] sortedArrayDesc(int inpArr[]) {
				int temp = 0;
				for (int i = 0; i < inpArr.length; i++) {
					for (int j = i + 1; j < inpArr.length; j++) {
						if (inpArr[i] < inpArr[j]) {
							temp = inpArr[i];
							inpArr[i] = inpArr[j];
							inpArr[j] = temp;
						}
					}
				}
				return inpArr;
			}
}
