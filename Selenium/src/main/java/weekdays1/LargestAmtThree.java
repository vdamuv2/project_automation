/*QUESTION: -------------------------------------------------------------------
Write a program to find the largest number amount the three.abstract
Input: Enter the number1: 5
	   Enter the number2: 3
	   Enter the number3: 9
Output: 9 is the largest number*/


/*PSEUDO CODE: -----------------------------------------------------------------

Add import details for Scanner to get input from use.
Create a class Greatest3numbers.
	Create a method getInput().
		Create an object for scanner scan.
		Declare 3 varaibles a,b and c and get 3 inputs from user by using scan.nextInt().
		Open if block with condition A greater than B & A greater than C
			print A
		else if B is greater than C
			print B
		else
		print C
		Close the class getInput()
	Create main method
	Create an object greatest for class Greatest3numbers
	Call the getInput() menthod using object.
	Close main method
Close class Greatest3numbers.

(or)

PSEUDO CODE: ---------------------------------------------------------------------------
	1. Create two function/method , one is to get input and another for logic and print the out.
	2. In function getNumber(), get the three inputs as integer from the user(i.e, Number1=5, Number2=3, Number3=9 ).
	3. Check the condition in function findGreatestNum(),
		if Number1 greater than Number2 and Number1 greater than Number3
			Print Number1;
		else if Number2 greater than Number1 and Number2 greater than Number3
			Print Number2;
		else if Number3 greater than Number1 and Number3 greater than Number2
			Print Number3;
	4. In main class create the object for class and call the created functions.*/
	
//FUNCTIONAL CODE: --------------------------------------------------------------------
	
package weekdays1;

import java.util.Scanner;

public class LargestAmtThree {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number1:");
		int number1 = scan.nextInt();
		System.out.println("Enter the number2:");
		int number2 = scan.nextInt();
		System.out.println("Enter the number3:");
		int number3 = scan.nextInt();
		
		if(number1>=number2)
		{
		if(number1>=number3)
			{
				System.out.println(number1 + " is the largest number");
			}
		else if(number2>=number3)
			{
				System.out.println(number2 + " is the largest number");
			}
			else System.out.println(number3 + " is the largest number");
		}
		scan.close();
		}
}
