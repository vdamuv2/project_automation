/*Java program to sum the numbers in an array. 
input = enter the values: 25,26,9,18,3,12
output = sum of the array = 93*/
package weekdays2;

public class SummArray {

	public static void main(String[] args){
		int[] values = {25,26,9,18,3,12};
		int sum = 0;
		int i =0;
		try
		{
		while(values[i]!=0)
		{
		sum = sum + values[i];
		i++;
		System.out.println("The sum of array: " +sum);
		}
//System.out.println(sum);
		}
		catch (Exception e)
		{
			System.out.println("ArrayIndexOutOfBoundsException is thrown");
		}
	}

}
