package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) throws InterruptedException {
		// tell where the chrome driver is available
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// create object for the chromedriver
		ChromeDriver driver = new ChromeDriver();

		// maximize the browser
		driver.manage().window().maximize();

		// Launch the url
		driver.get("http://leaftaps.com/opentaps");

		// finding the element for user name
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesMAnager");

		// finding the element for password
		driver.findElementById("password").sendKeys("crmsfa");

		// click on login
		driver.findElementByClassName("decorativeSubmit").click();

		// click on the link "CRM/SFA"
		driver.findElementByLinkText("CRM/SFA").click();

		 // click on the link "create lead"
		driver.findElementByLinkText("Create Lead").click();

		  //Fill all the fields 
		  driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		  driver.findElementById("createLeadForm_firstName").sendKeys("Vinz");
		  driver.findElementById("createLeadForm_lastName").sendKeys("Damz");
		  WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		  Select selectObj = new Select(source);
		  selectObj.selectByVisibleText("Employee");
		  driver.findElementById("createLeadForm_firstNameLocal").sendKeys("VinzDamz");
		  driver.findElementById("createLeadForm_personalTitle").sendKeys("Mrs");
		  driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Profile");
		  driver.findElementById("createLeadForm_annualRevenue").sendKeys("950000000");
		  WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		  Select selectObj1 = new Select(industry);
		  selectObj1.selectByValue("IND_FINANCE");
		  WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		  Select selectObj2 = new Select(ownership);
		  selectObj2.selectByIndex(3);
		  driver.findElementById("createLeadForm_sicCode").sendKeys("10025");
		  WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		  Select selectObj3 = new Select(marketing);
		  selectObj3.selectByIndex(6);
		  driver.findElementById("createLeadForm_lastNameLocal").sendKeys("damzvinz");
		  driver.findElementById("createLeadForm_departmentName").sendKeys("CS");
		  driver.findElementById("createLeadForm_numberEmployees").sendKeys("45000");
		  driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$");
		  driver.findElementById("createLeadForm_description").sendKeys("Greetings Everyone!!! Have a great day!!!");
		  driver.findElementById("createLeadForm_importantNote").sendKeys("important to be known");
		  driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600 001");
		  driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("5695");
		  driver.findElementById("createLeadForm_primaryEmail").sendKeys("vini@gmail.com");
		  driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9860014001");
		  driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("VVz");
		  driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://known.com");
		  driver.findElementById("createLeadForm_generalToName").sendKeys("Divi");
		  driver.findElementById("createLeadForm_generalAttnName").sendKeys("Divya");
		  driver.findElementById("createLeadForm_generalAddress1").sendKeys("Minna street");
		  driver.findElementById("createLeadForm_generalAddress2").sendKeys("2nd lane");
		  driver.findElementById("createLeadForm_generalCity").sendKeys("MN");
		  driver.findElementById("createLeadForm_generalPostalCode").sendKeys("12345");
		  driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("5666");
		  WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		  Select selectObj4 = new Select(state);
		  selectObj4.selectByIndex(4);
		  driver.findElementByName("submitButton").click();
		  
		  /*// dropdown using selectByVisibleText 
		  WebElement dropDown1 = driver.findElementById("createLeadForm_dataSourceId"); 
		  Select sel = new Select(dropDown1); 
		  sel.selectByVisibleText("Employee");
		  
		  // dropdown using selectByValue 
		  WebElement dropDown2 = driver.findElementById("createLeadForm_marketingCampaignId"); 
		  Select sel1 = new Select(dropDown2); sel1.selectByValue("CATRQ_AUTOMOBILE"); 
		  // dropdown using selectByIndex 
		  sel1.selectByIndex(4);
		 

		// print the values starts with C form the dropdown
		WebElement dropDown3 = driver.findElementById("createLeadForm_industryEnumId");
		Select sel2 = new Select(dropDown3);
		List<WebElement> list = sel2.getOptions();
		int sizeOfAllValues = list.size();
		for (int i = 0; i <= sizeOfAllValues-1; i++) {
			if(list.get(i).getText().startsWith("C")) {
				System.out.println(list.get(i).getText());
			}else {
				System.out.println("Invalid");
			}
		}
		//OR
		for(WebElement getAllC : list )
		{
			if(getAllC.getText().startsWith("C"))
					{
				System.out.println(getAllC.getText());
					}
		}*/
		
		
		//search and update the lead using Xpath only
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").clear();
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").sendKeys("vini");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		Thread.sleep(5000);
		driver.findElementByXPath("//a[contains(text(),'10506')]").click();
		driver.findElementByXPath("//a[contains(text(),'Edit')]").click();
		driver.findElementByXPath("//input[@id='updateLeadForm_firstName']").sendKeys("Vinitha");;
		driver.findElementByXPath("//input[@value='Update']").click();
	}

}
