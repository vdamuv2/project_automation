package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Xpath {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesMAnager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[contains(text(),'Merge Leads')]").click();
		//from parent to child:
		driver.findElementByXPath("//td/following-sibling::td/input[@id = 'partyIdTo' and @name = 'partyIdTo']/following::a").click();
//		driver.findElementByXPath("//tr/following::td/following-sibling::td/input[@id = 'partyIdTo']/following::a[1]").click();
		//from child to parent:
		driver.findElementByXPath("//input[@id ='partyIdTo' and @name = 'partyIdTo']/following::a/parent::td/table").sendKeys("Hello");
		
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		Thread.sleep(2000);
		//to get last name in first row
		WebElement getLastname = driver.findElementByXPath("(//table[@class = 'x-grid3-row-table'])[1]//td[2]/following::div[2]");
		System.out.println("Last name from the table is: " +getLastname.getText());
	
	}
	

}
