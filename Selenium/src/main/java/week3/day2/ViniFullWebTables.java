package week3.day2;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.formula.functions.Columns;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ViniFullWebTables {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("Chennai Central",Keys.TAB);
		driver.findElementById("txtStationTo").sendKeys("Palakkad",Keys.ENTER);
		
		/*//to verify if the checkbox is checked or not
		WebElement checkbox = driver.findElementByXPath("//td[@id='tdDateOnly']/following::input[@id='chkFirstLast']");
		if(checkbox.isSelected())
		{
			checkbox.click(); 
		}*/
		
		//for MANGALORE MAIL get the train available details for the whole week
		WebElement table = driver.findElementByXPath("//table[@class='DataTable DataTableHeader TrainList']/following::div[@id='divMainWrapper']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
//		List<WebElement> columns = table.findElements(By.tagName("td"));
		/*try{
			for(int i =0; i < rows.size(); i++)
		{
			WebElement eachRow = rows.get(i);
			List<WebElement> columns = eachRow.findElements(By.tagName("td"));
//			System.out.println(columns.get(1).getText()); //to get the 2nd column names
			if(columns.get(1).getText().contains("MANGALORE MAIL"))
			{
				System.out.println(columns.get(1).getText() +" Details:");
				System.out.println(rows.get(i).getText());
			}
		}
		}
	catch(Exception e)
	{
		System.out.println(e);
	}*/
		
		//give full details about the train using train number with the header details
		WebElement header = driver.findElementById("divTrainsListHeader");
		System.out.print(header.getText());
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the train number: ");
		int getTrainNumber = scan.nextInt();
//		System.out.println("Please enter the train name: ");
//		String getTrainName = scan.nextLine();
		for(int i =0; i< rows.size(); i++)
		{
			if(rows.get(0).equals(getTrainNumber))
			{
				System.out.println(rows.get(i));
			}
		}
	
	}
}
