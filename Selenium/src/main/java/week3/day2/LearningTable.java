package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearningTable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("Chennai Central",Keys.TAB);
		driver.findElementById("txtStationTo").sendKeys("Palakkad",Keys.ENTER);
//		driver.findElementByXPath("//td[@id = 'tdTrainFromTo']/input").click();
		driver.findElementByXPath("//table[@class='DataTable DataTableHeader TrainList']//tr/td[2]/a").click();
		
		WebElement fullTable = driver.findElementById("divMainWrapper");
		List<WebElement> rows = fullTable.findElements(By.tagName("tr"));
		WebElement column = fullTable.findElement(By.tagName("td"));
		
		for(int i =0; i < rows.size()-1; i++)
		{
			WebElement fullRows = rows.get(i);
			List<WebElement> specifiedColumn = fullRows.findElements(By.tagName("td"));
			System.out.println(specifiedColumn.get(1).getText());

		}
	}
	}

