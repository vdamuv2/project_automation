package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ViniFullXpath {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); //it waits for 30 secs until it finds for each elements in the webpage
		//Locators
		driver.get("http://leaftaps.com/opentaps");
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesMAnager",Keys.TAB);
		driver.findElementById("password").sendKeys("crmsfa");	//Locator 1 - Id **********************************
		driver.findElementByClassName("decorativeSubmit").click();	//Locator 2 - ClassName ***********************
		driver.findElementByLinkText("CRM/SFA").click(); 	//Locator 3 - LinkText ********************************
		driver.findElementByLinkText("Leads").click();	
		driver.findElementByLinkText("Create Lead").click();
		Thread.sleep(2000);
		driver.findElementById("createLeadForm_companyName").sendKeys("Wipro");
		driver.findElementByClassName("inputBox").sendKeys("Vinitha"); //change xpath
		
		//for select value from dropdown 
		WebElement dropdowm = driver.findElementByName("marketingCampaignId"); //Locator 4 - Name ******************
		Select sel = new Select(dropdowm);
		sel.selectByIndex(0);
		sel.selectByValue("CATRQ_AUTOMOBILE");
		sel.selectByVisibleText("eCommerce Site Internal Campaign"); //1st preference
		
		driver.findElementByPartialLinkText("Upload").click(); //Locator 5 - PartialLinkText ***********************
		List<WebElement> allImg = driver.findElementsByTagName("img"); //Locator 6 - TagName ***********************
		System.out.println(allImg.size());
		
		//Locator 7 - Xpath ****************************************************************************************
		//1.Attribute based xpath
		driver.findElementByXPath("//input[@id = 'uploadedFile']");
		//2.Partial xpath signature
		driver.findElementByXPath("//input[contains(@id,'uploadedFile')]"); //1st preference
		driver.findElementByXPath("//input[starts-with(@id,'uploadedFile')]");
		driver.findElementByXPath("//input[ends-with(@id,'uploadedFile')]");
		//3a.Exact Text matches
		driver.findElementByXPath("//a[text()='Merge Leads']");
		//3b.Partial text matches
		driver.findElementByXPath("//a[contains(text(),'Merge Leads')]");
		//Search only text nodes
		driver.findElementByXPath("//a[text()[contains(.,'Merge')]]"); //OR
		driver.findElementByXPath("//*[text()[contains(.,'Merge')]]");
		
		//Locator 7 - Xpath with relationship **********************************************************************
		//1. Parent to child
		driver.findElementByXPath("//table/tbody/tr[2]//following::input");
		//2. Parent to all child
		driver.findElementByXPath("//table/tbody/tr[2]//following::*");
		//3. Grandparent to child
		driver.findElementByXPath("//div[@id='center-content-column']/div[2]//table[@class='twoColumnForm']/tbody/tr/following-sibling::tr//td/input");
		//4. From child to parents
		driver.findElementByXPath("//input[@class='smallSubmit']/parent::td/preceding-sibling::td/parent::tr/preceding-sibling::tr/parent::tbody/parent::table");
		//5. From child to ancestors
		driver.findElementByXPath("//input[@class='smallSubmit']/ancestor::table");
		//6a. Siblings
		driver.findElementByXPath("//table[@class='twoColumnForm']/tbody/tr/following::tr");
		//6b. Following Siblings
		driver.findElementByXPath("//table[@class='twoColumnForm']/tbody/tr/following-sibling::tr");
		//6c. Preceding Siblings
		driver.findElementByXPath("//input[@class='smallSubmit']/parent::td/preceding-sibling::td/parent::tr/preceding-sibling::tr"); //OR
		driver.findElementByXPath("//input[@class='smallSubmit']/parent::td/preceding-sibling::td/parent::tr/preceding-sibling::*");
		//all inputs tag in one container and select the 3rd one. for last use last() method. 
		driver.findElementByXPath("(//input)[3]");
		driver.findElementByXPath("(//input)[last()]");
		driver.findElementByXPath("(//input)[last()-1]"); //for last before one
		
		/*Note: 
			from top(parent) to down (child) ---> following
			from  down (child) to top(parent)---> preceding
			from child to child ---> sibling
			* ---> all
			* ---> last() -to get the last */
		
		//findElementBy
		driver.findElement(By.tagName("a"));
		driver.findElement(By.className("a"));
		driver.findElement(By.id("a"));
		driver.findElement(By.name("a"));
		driver.findElement(By.linkText("a"));
		driver.findElement(By.partialLinkText("a"));
		driver.findElement(By.xpath("a"));
		driver.findElement(By.cssSelector("a"));
	
		//findElementsBy
		driver.findElements(By.tagName("a"));
		driver.findElements(By.className("a"));
		driver.findElements(By.id("a"));
		driver.findElements(By.name("a"));
		driver.findElements(By.linkText("a"));
		driver.findElements(By.partialLinkText("a"));
		driver.findElements(By.xpath("a"));
		driver.findElements(By.cssSelector("a"));
		
		
		//Locator 8 - CSS (StyleSheet) (Yet to teach) **************************************************************
		
	}
		
}
