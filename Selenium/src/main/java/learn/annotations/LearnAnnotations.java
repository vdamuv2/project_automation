package learn.annotations;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class LearnAnnotations {
//  @Test(dataProvider = "dp")
//  public void f(Integer n, String s) {
//  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("4.get into office");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("6. get out of office");
  }


//  @DataProvider
//  public Object[][] dp() {
//    return new Object[][] {
//      new Object[] { 1, "a" },
//      new Object[] { 2, "b" },
//    };
//  }
  @BeforeClass
  public void beforeClass() {
	  System.out.println("3. formals");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("9 . casuals");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("2. get ready");
  }
  @Test
	 public void mainWork() {
		  System.out.println("5 . Working");
	  }
  @AfterTest
  public void afterTest() {
	  System.out.println("10 . bed time");
  }
  @Test
	 public void mainBreak() {
		  System.out.println(" 7. take break");
	  }
  @Test
	 public void mainFunction() {
		  System.out.println(" 8. dancing");
	  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println(" 1. wake up");
  }
  @AfterSuite
  public void afterSuite() {
	  System.out.println(" 11. sleep");
  }

}
