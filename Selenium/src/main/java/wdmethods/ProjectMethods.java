package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;

public class ProjectMethods extends SeMethods {

	public static void main(String[] args) {
	
	}
	@DataProvider(name = "fetchData")
	public String[][] getData() throws IOException
	{
		return ReadExcel.getExcelFile(exFileName);
		/*String[][] data = new String[2][3];
		data[0][0] = "TCS";
		data[0][1] = "Vinitha";
		data[0][2] = "Vinayan";
		
		data[1][0] = "Wipro";
		data[1][1] = "Vinitha";
		data[1][2] = "Damodharan";
		return data;*/
	}
	public void login(String bro, String url)
	{
//		startApp("chrome", "http://leaftaps.com/opentaps/");
		startApp(bro, url);
		WebElement eleUsername1 = locateElement("id", "username");
		type(eleUsername1, "DemoSalesManager");
		WebElement elePassword1 = locateElement("id", "password");
		type(elePassword1, "crmsfa");
		WebElement eleLogin1 = locateElement("class", "decorativeSubmit");
		click(eleLogin1);
		WebElement eleCRM = locateElement("linkText", "CRM/SFA");
		click(eleCRM);
	}
}
