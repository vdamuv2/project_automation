package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

import util.Reporter;

public class SeMethods extends Reporter implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	@Override
	public void startApp(String browser, String url) {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get(url); 
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		reportSteps("pass", "The browser "+browser+" launched successfully"); 
		takeSnap();
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linkText": return driver.findElementByLinkText(locValue);
			case "partialLink": return driver.findElementByPartialLinkText(locValue);
			case "tagName": return driver.findElementByTagName(locValue);
			}
		} catch (NoSuchElementException e) {
			reportSteps("fail", "The element not found exception");
			takeSnap();
		} catch (WebDriverException e) {
			reportSteps("fail", "Unknown Exception occured!!!");
			takeSnap();
		}
		
		
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue); // also can mention "return.findElementById(locValue);"
		} catch (NoSuchElementException e) {
			reportSteps("fail", "The 'id' element is not found");
			takeSnap();
		
		}
		return null;
		
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportSteps("pass", "The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			reportSteps("fail", "The data "+data+" not enter successfully!!!");
		} finally {
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		reportSteps("pass", "The element "+ele+" is clicked"); 
		takeSnap();
	}
	
	public void clickWithOutSnap(WebElement ele) {
		ele.click();
		reportSteps("pass", "The element "+ele+" is clicked"); 
	}
	
	@Override
	public String getText(WebElement ele) {
//		try {
//			System.out.println(ele.getText());
//		} catch (Exception e) {
//			System.out.println("Couldn't get the text");
//		}
//		finally
//		{
//			takeSnap();
//		}
//		return null;
		
		String text = ele.getText();
		return text;
	}
	
	@Override
	public void selectDropDownUsingText(WebElement ele, String value)  {
	try {
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		reportSteps("pass", "The value is selected from the dropdown");
		
	} catch (Exception e) {
		reportSteps("fail", "The value is not found in the web!!!");
	} 
	finally
	{
		takeSnap();
	}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
	try {
		Select sel = new Select(ele);
		sel.selectByIndex(index);
		reportSteps("pass", "The given index's value is selected from dropdown");
	} catch(Exception e)
	{
		reportSteps("fail", "The index is not valid!!!");
	}
	finally {
		takeSnap();
	}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String values) {
	try {
		Select sel = new Select(ele);
		sel.selectByValue(values);
		reportSteps("pass", "The value is selected from dropdown");
	} catch (UnexpectedTagNameException e) {
		reportSteps("fail", "UnExpected tag is found!!!");
	}catch (Exception e) {
		reportSteps("fail", "Unknown exception is found!!!");
	}
	finally {
		takeSnap();
	}
	}
	
	public void selectDropDownUsingTextValueIndex(WebElement ele, String value, String sel)  {
		try
		{
		Select dd = new Select(ele);
		if(sel.equalsIgnoreCase("Visible"))
				{
			dd.selectByVisibleText(value);
				}if(sel.equalsIgnoreCase("Value"))
				{
			dd.selectByValue(value);
				}else if(sel.equalsIgnoreCase("Index"))
				{
			dd.selectByIndex(Integer.parseInt(value));
				}
				reportSteps("pass", "Value is selected from dropdown");
		}
		catch(Exception e)
		{
			reportSteps("fail", "Value is not selected");
		}
		finally {
			takeSnap();
		}
	}
	
	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn = false;
		try {
			String title = driver.getTitle();
			if(title.equals(expectedTitle)){
				reportSteps("pass", "The title is matched" +expectedTitle);
			bReturn = true; }
			else { reportSteps("fail", "Title is not matched"); }
		} catch (Exception e) {
			reportSteps("fail", "Title is not found");
		}
		return bReturn; // to get true or false based on the output v get
	}
// (OR)
//	public boolean verifyTitle(String expectedTitle) {
//		try {
//			String title = driver.getTitle();
//			if(title.equals(expectedTitle)){
//			System.out.println("The title is matched" +expectedTitle);
//			return true; }
//			else { System.out.println("Title is not matched"); }
//		} catch (Exception e) {
//			System.out.println("Title is not found");
//		}
//		return false;
//	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		if(ele.getText().equals(expectedText))
		{
			reportSteps("pass", "Excepted text is verified");
		}else { reportSteps("fail", "Text verification is failed");}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().contains(expectedText))
			{
				reportSteps("pass", "The text contains the expected text" +expectedText);
			}
		} catch (Exception e) {
			reportSteps("fail", "Unknown exception found");
		}finally {
			takeSnap();
		}
	}
	
	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).equals(value))
		{
			reportSteps("pass", "The Attribute is matched");
		}else {
			reportSteps("fail", "Attribute not matched");
		takeSnap();
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).contains(value))
		{
			reportSteps("pass", "The partial attribute is matched");
		}else {
			reportSteps("fail", "The partial attribute is not matched");
		takeSnap();
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
	if(ele.isSelected())
	{
		reportSteps("pass", "The value is selected");
	}else
	{
		reportSteps("fail", "The value is not selected");
	takeSnap();
	}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed())
		{
			reportSteps("pass", "The value is displayed");
		}else {
			reportSteps("fail", "The value is not displayed");
			takeSnap();
		}

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(windowHandles);
		driver.switchTo().window(listOfWindows.get(index));
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			
		} // we write this logic to handle the exception. 
		// This UnHandledAlertException is: if we don't attend/handle the exception. so we cant use this exception here
		// we can't take snap for Alert

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
	System.out.println("NoAlertPresentException");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	} 

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			reportSteps("pass", "The broswer is closed"); // System.out.println(); and screenshot is not possible since the driver is closed
		} catch (WebDriverException e) {
			reportSteps("fail", "Broswer is unreachable!!!");
		}
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
		} catch (Exception e) {
			reportSteps("fail", "Technical Error!!!");
		}
	}

	@Override
	public void auiActions(String keyboard, String keyAction, String mouseAction) {
//		Actions builder = new Actions(driver);
//		builder.keyboard(keys.keyAction)
//		.mouseAction.perform();
		
		
		}
	@Override
	public void waitTime() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			reportSteps("fail", "InterruptedException is thrown");
		}
		
	}

	@Override
	public void handleMultipleWindows(int i) {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listAllWindows = new ArrayList<>();
		listAllWindows.addAll(windowHandles);
		driver.switchTo().window(listAllWindows.get(i));
		
	}
	
		
	}

	

