/* An Armstrong number of three digits is an integer such that the sum of the cubes of its digits is equal to the number itself. 
 For example, 371 is an Armstrong number since 3**3 + 7**3 + 1**3 = 371.
 
 *Pusedo code:
1) iterate 100 to 1000
2) create two temporary integers to manipulate ( one for current iterate number and another for temporary number) 
3) apply Armstrong formulae (sum of cubes) 
4) check if current number is equal to sum,  if yes print it
5) refresh sum
*/


package weekdays3;

import java.util.Scanner;

public class Amstrong {

	public static void main(String[] args) 
	{
		int sum = 0;
		System.out.print("Armstrong numbers from 100 to 1000:");
		for (int i = 100; i <= 1000 ; i++)
		{
			int num = i;
			while (num > 0)
			{
			int reminder = num % 10;
				sum = sum + (reminder*reminder*reminder);
				num = num / 10;
			}
	 
			if (sum == i)
			{
				System.out.print(i + " ");
			}
			sum = 0;
		}
	}

}
