package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static void main(String[] args) throws IOException 
	{
		
		XSSFWorkbook myBook = new XSSFWorkbook("./data/CreateLead.xlsx"); //CreateLead excel is opened
		XSSFSheet sheet = myBook.getSheet("CL"); //Sheet1 'CL' is identified
		
		int countRow = sheet.getLastRowNum(); //no of rows count starting from index 0
		System.out.println("Row count is: " +countRow);
		int countsRow = sheet.getPhysicalNumberOfRows(); //no of rows count starting from index 1
		System.out.println("Column count using getPhysicalNumberOfRows is : " +countsRow);
		short countsColumn = sheet.getRow(0).getLastCellNum(); //no of columns count starting from index 0
		System.out.println("Column count using getLastCellNum is : " +countsColumn);
		
		for (int i = 1; i <=countRow; i++) 
		{
			XSSFRow row = sheet.getRow(i); // get particular row from sheet
			for (int j = 0; j < countsColumn; j++) 
			{
				XSSFCell cell = row.getCell(j); // get particular column/cell from row
				String cellValue = cell.getStringCellValue();  // get value from the cell
				System.out.println(cellValue); // print the value
			}
		}
		
	}

}
