package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdmethods.ProjectMethods;


public class TC003_DeleteLead extends ProjectMethods
{
	
	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}
	
//	@Test(priority = 3)
	@Test(dataProvider = "fetchData")
	public void editLead(String idNum)
	{
		WebElement eleLead = locateElement("linkText", "Leads");
		click(eleLead);
		
		WebElement eleLink = locateElement("linkText", "Find Leads");
		click(eleLink);
		
		WebElement firstName = locateElement("name", "id");
		type(firstName, idNum);
		
		WebElement eleButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButton);
		
		WebElement elefirstLink = locateElement("linkText", "par");
		click(elefirstLink);
		
		WebElement eleDelete = locateElement("linkText", "Delete");
		click(eleDelete);
	
	}
	@DataProvider(name = "fetchData")
	public String[][] getData()
	{
		String[][] data = new String[1][1];
		data[0][0] = "10077";	
		data[1][0] = "10078";
		
		return data;
	}
	@AfterMethod
	public void CloseApp()
	{
		closeBrowser();
	}
}
