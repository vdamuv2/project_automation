package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import wdmethods.ProjectMethods;


public class TC002_EditLead extends ProjectMethods 
{
	@Parameters({"tcNameE" , "tDesc", "tAuthor",  "tCategory"})
	@BeforeTest
	public void setData(String testNameE, String tesDesc,  String tAuthor, String tcategory) {
		testcaseName = testNameE;
		testDesc = tesDesc;
		author = tAuthor;
		category = tcategory;
	}

	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}
	
	
//	@Test(priority = 4)
	@Test(dataProvider ="fetchData" )
	public void editLead(String idNumber, String editName)
	{
	
	WebElement eleLead = locateElement("linkText", "Leads");
	click(eleLead);
	
	WebElement eleLink = locateElement("linkText", "Find Leads");
	click(eleLink);
	
	WebElement firstName = locateElement("name", "id");
	type(firstName, idNumber);
	
	WebElement elefirstLink = locateElement("linkText", "Test1");
	click(elefirstLink);
	
	WebElement eleEdit = locateElement("linkText", "Edit");
	click(eleEdit);
	
	WebElement editCompany = locateElement("id", "updateLeadForm_companyName");
	type(editCompany, editName);
	
	WebElement eleUpdate = locateElement("name", "submitButton");
	click(eleUpdate);
	
	}
	
	@DataProvider(name = "fetchData")
	public String[][] getData()
	{
		String[][] data = new String[2][2];
		data[0][0] = "10070";
		data[0][1] = "Hitech";
		
		data[1][0] = "10080";
		data[1][1] = "HelloCompany";
		
		return data;
	}
	
	@AfterMethod
	public void CloseApp()
	{
		closeBrowser();
	}
}
