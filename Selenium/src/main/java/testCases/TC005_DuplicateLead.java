package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods{
	
	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}
	
//	@Test(priority = 1)
	@Test(dataProvider = "fetch")
	public void duplicateLead(String idNum, String text)
	{
		WebElement eleLead = locateElement("linkText", "Leads");
		click(eleLead);
		
		WebElement eleLink = locateElement("linkText", "Find Leads");
		click(eleLink);
		
		WebElement firstName = locateElement("name", "id");
		type(firstName, idNum);
		WebElement eleButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButton);
		
		WebElement elefirstLink = locateElement("linkText", "Viji");
		click(elefirstLink);
		
		WebElement eleDuplicate = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(eleDuplicate);
		
		waitTime();
		
		WebElement dropdown = locateElement("name", "marketingCampaignId");
		selectDropDownUsingText(dropdown, "Automobile");

		WebElement dropdown2 = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(dropdown2, 3);
		
		WebElement importantNotes = locateElement("name", "importantNote");
		type(importantNotes, text);

		WebElement eleSubmit = locateElement("name", "submitButton");
		click(eleSubmit);

	}
	@DataProvider(name = "fetch")
	public String[][] getData()
	{
		String[][] data = new String[2][2];
		data[0][0] = "10080";
		data[0][1] = "Hello Everyone!!! Good Morning!!!";
		
		data[1][0] = "10071";
		data[1][1] = "Hello !!! Good Morning!!!";
	
		return data;
		
	}
	@AfterMethod
	public void CloseApp()
	{
		closeBrowser();
	}

}
