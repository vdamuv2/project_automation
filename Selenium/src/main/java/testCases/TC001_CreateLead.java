package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import wdmethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods {

	@Parameters({"tcName" , "tDesc", "tAuthor",  "tCategory", "eclFileName"})
	@BeforeTest
	public void setData(String testName, String tesDesc,  String tAuthor, String tcategory, String eclFileName) {
		testcaseName = testName;
		testDesc = tesDesc;
		author = tAuthor;
		category = tcategory;
		exFileName = eclFileName;
	}

	@Parameters({ "browser", "url" })
	@BeforeMethod
	public void loginApp(String bro, String url) {
		login(bro, url);
	}

//	@Test(invocationCount = 2)
//	@Test(invocationCount = 2, invocationTimeOut = 8000)
//	@Test(priority = 5)
	@Test(dataProvider = "fetchData")
	public void createL(String comName, String fName, String lName) {
		WebElement eleLink = locateElement("linkText", "Create Lead");
		click(eleLink);

		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, comName);

		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, fName);

		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, lName);

		WebElement dropdown = locateElement("name", "marketingCampaignId");
		selectDropDownUsingText(dropdown, "Automobile");

		WebElement dropdown2 = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(dropdown2, 3);

		WebElement dropdown3 = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingValue(dropdown3, "OWN_PROPRIETOR");

		WebElement importantNotes = locateElement("name", "importantNote");
		type(importantNotes, "Hello Everyone!!! Good Morning!!!");

		WebElement eleButton = locateElement("name", "submitButton");
		click(eleButton);

	}
	
	@AfterMethod
	public void CloseApp() {
		closeBrowser();
	}

}
