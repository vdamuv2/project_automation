/*1. List of employee Id in list with duplicates
   print in sorting order
   
   2. Remove duplicates in list and print in sorting
   order
   
   4. Write a code to remove duplicates from string
   input - goodday
   output - goday */

package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListSort {

	public static void main(String[] args) {
		ListSort ls = new ListSort();
//		ls.sortDuplicates();
//		removeDuplicates();
		ls.removeDupNString();
	}
	
	public void sortDuplicates()
	{
		System.out.println("This is sort the duplicate list in list ");
		List<Integer> empId = new ArrayList<>();
		empId.add(111);
		empId.add(112);
		empId.add(114);
		empId.add(154);
		empId.add(113);
		empId.add(122);
		empId.add(000);
		empId.add(117);
		empId.add(120);
		empId.add(111);
		empId.add(113);
		empId.add(113);
		empId.add(117);
		empId.add(112);
		empId.add(155);
		empId.add(777);
		empId.remove(000);
		System.out.println(empId.size());
		System.out.println(empId.contains(111));
		System.out.println(empId.get(3));
		Collections.sort(empId);
		for(int value : empId)
		{
			System.out.println(value);
		}
	}
	public static void removeDuplicates()
	{
		System.out.println("This is remove duplicate and sort of list using Set ");
		List<Integer> empId = new ArrayList<>();
		empId.add(111);
		empId.add(112);
		empId.add(114);
		empId.add(154);
		empId.add(113);
		empId.add(122);
		empId.add(000);
		empId.add(117);
		empId.add(120);
		empId.add(111);
		empId.add(113);
		empId.add(113);
		empId.add(117);
		empId.add(112);
		empId.add(155);
		empId.add(777);
		empId.remove(000);
		System.out.println(empId.size());
		System.out.println(empId.contains(111));
		System.out.println(empId.get(3));
		Set<Integer> filterDuplicates = new TreeSet<>();
		filterDuplicates.addAll(empId);
		System.out.println(filterDuplicates); // output will be without any duplicate values and in sorted order since i used TreeSet(to get the output in sorted order(as per ASCII))

	}

	public void removeDupNString()
	{
//		System.out.println("This is remove duplicate from string");
		String input = "goodday";
		char[] c = input.toCharArray();
		List<Character> ls = new ArrayList<>(); // object should be created once so don't put this inside any loops
		Set<Character> removeDup = new LinkedHashSet<>();
		for(char ch : c)
		{
//			System.out.println("Before removing duplciate:"+ch);
			ls.add(ch);
		}
		removeDup.addAll(ls);
		System.out.println(removeDup);
	}
}
