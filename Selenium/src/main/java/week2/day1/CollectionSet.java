/*Create an object for the list with 5 phone numbers with duplicates,
then remove the duplicates from the list by converting the
list to set and then verify if the list has only the unique values.*/
package week2.day1;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class CollectionSet {

	public static void main(String[] args) 
	{
		List<Integer> phone = new ArrayList<>();
		phone.add(987654222);
		phone.add(987654213);
		phone.add(987654210);
		phone.add(987654210);
		phone.add(987654213);
		System.out.println("The size of the list: " +phone.size());
		Set<Integer> setPhone = new LinkedHashSet<>();
		setPhone.addAll(phone);
//		System.out.println("The size of the set: " +setPhone.size());
		System.out.println(setPhone);
		
	}

}
