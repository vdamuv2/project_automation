/*3. Take a string as input change all odd index in    Uppercase even index Lowercase
  input = Testleaf
  output = tEsTlEaF*/
  
  package week2.day1;

import java.util.ArrayList;
import java.util.List;

public class OddIndexNUpper {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		String input = "Testleaf";
		char[] c = input.toCharArray();
		for(char ch : c)
		{
//			System.out.println(ch);
			List<Character> ls = new ArrayList<>();
			ls.add(ch);
			for(int i=0; i<ls.size();i++)
			{
				if(ls.get(i)%2==0)
				{
					System.out.print(ls.get(i).toLowerCase(ch));
				}
				else 
				{
					System.out.print(ls.get(i).toUpperCase(ch));
				}
			}
		}
	}

}
